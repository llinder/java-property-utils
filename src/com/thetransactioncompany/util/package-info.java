/**
 * This package provides a Java utility for typed retrieval of 
 * {@code java.util.Properties} as boolean, integer, floating point, string and 
 * enum values.
 *
 * <p>System requirements:
 *
 * <ul>
 *     <li>Java 1.5 or later (for generics and enum support)
 * </ul>
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ ($version-date$)
 */
package com.thetransactioncompany.util;
