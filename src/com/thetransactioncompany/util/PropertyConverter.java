package com.thetransactioncompany.util;


import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;


/**
 * Static utility methods for converting servlet and context initialisation
 * parameters to Java properties.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2012-07-15)
 */
public class PropertyConverter {


	/**
	 * Converts the servlet initialisation parameters (typically specified 
	 * in the {@code web.xml} file) to a Java properties hashtable. The 
	 * parameter names become property keys.
	 *
	 * <p>Note regarding {@code web.xml} markup: The servlet initialisation 
	 * parameters have an XML tag {@code <init-param>} and are defined 
	 * within the {@code <servlet>} element.
	 *
	 * @param config The servlet configuration.
	 *
	 * @return The servlet initialisation parameters as Java properties.
	 */
	public static Properties getServletInitParameters(final ServletConfig config) {
	
		Properties props = new Properties();
	
		Enumeration en = config.getInitParameterNames();
		
		while (en.hasMoreElements()) {
			
			String key = (String)en.nextElement();
			String value = config.getInitParameter(key);
			
			props.setProperty(key, value);
		}
	
		return props;
	}
	
	
	/**
	 * Converts the servlet context initialisation parameters (typically 
	 * specified in the {@code web.xml} file) to a Java properties 
	 * hashtable. The parameter names become property keys.
	 *
	 * <p>Note regarding {@code web.xml} markup: The context parameters 
	 * have an XML tag {@code <context-param>} and are defined within the 
	 * {@code <web-app>} element.
	 *
	 * @param ctx The servlet context.
	 *
	 * @return The servlet context parameters as Java properties.
	 */
	public static Properties getServletCtxInitParameters(final ServletContext ctx) {
	
		Properties props = new Properties();
	
		Enumeration en = ctx.getInitParameterNames();
		
		while (en.hasMoreElements()) {
			
			String key = (String)en.nextElement();
			String value = ctx.getInitParameter(key);
			
			props.setProperty(key, value);
		}
	
		return props;
	}
}
